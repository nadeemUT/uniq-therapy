@include('layout.header')
  <!-- Navigation -->
  <!-- Header -->
  <header class="bg-primary py mb-2">
      <div class="my-header">
            <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-lg-12">
          <h1 class="display-6 text-white mt-5 mb-2">Disease might be rare, access to cure should not be</h1>
          <p class="lead mb-5 text-white-50">Explore the options with us</p>
            
            <!-- <div class="topnav"> -->
            <form method="post" action="/search" enctype="multipart/form-data">
              {{ csrf_field() }} 

              <input type="text" class="searchbar typeahead" name="query" placeholder="Search..." style="padding: 10px;width: 77%;">
              <input type="submit" value="Submit" style="padding:10px">
              
            </form>  
            <!-- </div> -->
            
              
            
        </div>
      </div>
    </div>
      </div>
  </header>

  <!-- Page Content -->
  <div class="container">

    <div class="row">
      <div class="col-md-8 mb-5">
        <h2>What We Do</h2>
        <hr>

        <p>We are a team dedicated to connecting patients with rare diseases to promising medical trials.</p>
        <p>If you or your relative is affected with a rare disease and you are looking for trials, fill out the form below.</p>
        <a class="btn btn-primary btn-lg" href="/contact">Sign Up</a>

      </div>
      <div class="col-md-4 mb-5">
        <h2>Contact Us</h2>
        <hr>
        <address>
          <strong>UniQ Therapies</strong>
            <br>Ülikooli 17
          <br>Tartu, Estonia
          <br>
        </address>
        <address>

          <abbr title="Phone">Phone:</abbr>
          +372 583 593 53
          <br>

          <abbr title="Email">Email:</abbr>
          <a href="mailto:#">UniqTherapies@gmail.com</a>
        </address>
      </div>
    </div>
    <!-- /.row -->

    <div class="row">
     
      @for( $i=0; $i<3; $i++)
      <div class="col-md-4 mb-5">
        <div class="card h-100">
          <img class="card-img-top" src="{{URL::asset('/images/hospital')}}{{$i+1}}{{('.jpg')}}" alt="" style="width:348px;height:225px">
          <div class="card-body">
            <h4 class="card-title">{{ $data[$i]->name }}</h4>
            <p class="card-text">{{ $data[$i]-> short_description }}</p>
          </div>
          <div class="card-footer">
            <a href="{{ url('detail/'.$data[$i]->disease_id) }}" class="btn btn-primary btn-primary-new">Find Out More!</a>
          </div>
        </div>
      </div>
      @endfor
    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->
  
  @include('layout.footer')