@include('layout.header')
  <!-- Page Content -->
  <div class="container" style="margin-bottom:10px">

    <div class="row">

      <div class="col-lg-2">
      
      </div >
      <!-- /.col-lg-3 -->

      <div class="col-lg-8">

        <div class="card mt-4">
          <img class="card-img-top img-fluid" src="{{URL::asset('/images/hospital')}}{{$data->disease_id}}{{('.jpg')}}" alt="">
          <div class="card-body">
            <h3 class="card-title">{{ $data -> name }}</h3>
            <p class="card-text">{{ $data -> full_description }}</p>
            <hr>
            <a href="/contact" class="btn btn-success">Register as Patient</a>
          </div>
        </div>
        <!-- /.card -->
        <!-- /.card -->

      </div>
	  <div class="col-lg-2">
      
      </div >
      <!-- /.col-lg-9 -->

    </div>

  </div>
  <!-- /.container -->
  @include('layout.footer')