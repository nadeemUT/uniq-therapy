﻿@include('layout.header')

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134128200-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-134128200-1');
</script>

<header style="margin-top:30px">
    
    </header>
  <div class="container">
    <div class="row">

        <div class="col-md-5 mb-5 border-light p-5" style="margin-right:2px">

        UniQ Therapies is a portal that connects people who have rare diseases with clinical
         trials carried out by trusted hospitals and research facilities. If you wish to be notified when 
         we launch, please fill out the form.

        </div>
        <div class="col-md-6 mb-5 border border-light p-5">
        
        
        @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                  <strong>{{ $message }}</strong>
          </div>
        @endif


          @if ($message = Session::get('error'))
          <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                  <strong>{{ $message }}</strong>
          </div>
          @endif
        <h4>Please register yourself</h4>
        <form method="post"  enctype="multipart/form-data">
        {{ csrf_field() }}
          <div class="form-group">
            <label for="name">Full Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Full name">
          </div>
          <div class="form-group">
            <label for="email" style="text-align:left">Email address</label>
            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
          </div>
          
<!--           
          <div class="form-group">
            <label for="exampleInputFile">File input</label>
            <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
            <small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>
          </div> -->
          
          
          <button type="submit" class="btn btn-primary">Register</button>
        </form>
        </div>
        
      </div>
    </div>  
    <!-- /.row -->
  

  </div>
  <!-- /.container -->

  @include('layout.footer')
