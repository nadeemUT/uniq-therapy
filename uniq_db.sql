-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Loomise aeg: Veebr 08, 2019 kell 12:26 PL
-- Serveri versioon: 10.1.37-MariaDB
-- PHP versioon: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Andmebaas: `uniq_db`
--

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `diseases`
--

CREATE TABLE `diseases` (
  `disease_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `therapies` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Andmete tõmmistamine tabelile `diseases`
--

INSERT INTO `diseases` (`disease_id`, `title`, `short_description`, `full_description`, `therapies`, `created_at`, `updated_at`) VALUES
(1, 'Rett Syndrome', 'Rett syndrome is a progressive neurodevelopmental disorder that almost exclusively affects females. Only in rare cases are males affected', 'Rett syndrome is a progressive neurodevelopmental disorder that almost exclusively affects females. Only in rare cases are males affected. Infants with Rett syndrome generally develop normally for about 7 to 18 months after birth. At this point, they lose previously acquired skills (developmental regression) such as purposeful hand movements and the ability to communicate. Additional abnormalities occur including impaired control of voluntary movements (ataxia) and the development of distinctive, uncontrolled hand movements such as hand clapping or rubbing. Some children also have slowing of head growth (acquired microcephaly), Affected children often develop autistic-like behaviors, breathing irregularities, feeding and swallowing difficulties, growth retardation, and seizures. Most Rett syndrome cases are caused by identifiable mutations of the MECP2 gene on the X chromosome and can present with a wide range of disability ranging from mild to severe. The course and severity of Rett syndrome is determined by the location, type and severity of the MECP2 mutation and the process of random X-inactivation (see Causes section below). Therefore, two girls of the same age with the same mutation can appear significantly different', '', NULL, NULL),
(2, 'Familial Mediterranean Fever', 'Familial Mediterranean fever (FMF) is an inherited autoinflammatory disease characterized by recurrent episodes (attacks) of fever and acute inflammation of the membranes lining the abdomen, joints, and lungs', 'Familial Mediterranean fever (FMF) is an inherited autoinflammatory disease characterized by recurrent episodes (attacks) of fever and acute inflammation of the membranes lining the abdomen, joints, and lungs. In some cases, affected individuals may develop skin rashes (erysipelas like erythema) affecting the lower legs. Less often, inflammation of the membrane lining the heart or covering the brain and spinal cord may occur. Some individuals may develop a serious condition known as amyloidosis, in which certain proteins called amyloid accumulates in various tissues of the body. In FMF, amyloid accumulates in the kidneys (renal amyloidosis) where it can impair kidney function potentially result in life-threatening complications such as kidney failure. The specific symptoms and severity of FMF are highly variable. Some individuals develop amyloidosis, but none of the other symptoms associated with FMF. These cases are sometimes referred to as FMF type 2. FMF is caused by mutations of the MEditerranean FeVer (MEFV) gene and is basically inherited as an autosomal recessive trait. Some cases of dominant inheritance have been described', '', NULL, NULL),
(3, 'Down Syndrome', 'In every cell in the human body there is a nucleus, where genetic material is stored in genes.', 'In every cell in the human body there is a nucleus, where genetic material is stored in genes. Genes carry the codes responsible for all of our inherited traits and are grouped along rod-like structures called chromosomes. Typically, the nucleus of each cell contains 23 pairs of chromosomes, half of which are inherited from each parent. Down syndrome occurs when an individual has a full or partial extra copy of chromosome 21.\r\n            This additional genetic material alters the course of development and causes the characteristics associated with Down syndrome. A few of the common physical traits of Down syndrome are low muscle tone, small stature, an upward slant to the eyes,\r\n            and a single deep crease across the center of the palm – although each person with Down syndrome is a unique individual and may possess these characteristics to different degrees, or not at all.According to the Centers for Disease Control and Prevention, approximately one in every 700 babies in the United States is born with Down syndrome, making Down syndrome the most common chromosomal condition. About 6,000 babies with Down syndrome are born in the United States each year.\r\n            Down syndrome occurs in people of all races and economic levels, though older women have an increased chance of having a child with Down syndrome. A 35 year old woman has about a one in 350 chance of conceiving a child with Down syndrome, and this chance increases gradually to 1 in 100 by age 40. At age 45 the incidence becomes approximately 1 in 30. The age of the mother does not seem to be linked to the risk of translocation.\r\n            Since many couples are postponing parenting until later in life, the incidence of Down syndrome conceptions is expected to increase. Therefore, genetic counseling for parents is becoming increasingly important. Still, many physicians are not fully informed about advising their patients about the incidences of Down syndrome, advancements in diagnosis, and the protocols for care and treatment of babies born with Down syndrome.', '', NULL, NULL),
(4, 'Dengue Fever', 'Dengue Fever is an acute viral infection characterized by fever. It is caused by a bite from mosquitoes carrying dengue virus.', 'Dengue Fever is an acute viral infection characterized by fever. It is caused by a bite from mosquitoes carrying dengue virus. The primary form of Dengue Fever is characterized by a skin rash and a high fever with severe pain in the head and muscles. Other symptoms may include shaking chills, diarrhea, and vomiting. Bouts of extreme exhaustion may last for months after the initial symptoms.\r\n\r\n            The secondary forms of this disorder are called Dengue Hemorrhagic Fever and Dengue Shock Syndrome. These usually are caused by a secondary infection with a different type of Dengue virus (Type 2), but may also be caused by the same virus that causes Dengue Fever. Several days after onset other symptoms may include fever, bleeding under the skin, red spots on the legs, and bleeding into the intestines. A marked fall in blood pressure (shock) occurs in very severe cases.\r\n            Dengue Fever is caused by a virus that belongs to a family of viruses known as Flaviviruses. There is no vaccine, so travelers going to parts of the world where it is endemic during Dengue season (July to November) are urged to protect themselves from mosquito bites by wearing protective clothing and mosquito repellant.', '', NULL, NULL),
(5, 'Arachnoiditis', 'Arachnoiditis is a disease characterized by an acute inflammatory stage that occurs in the dura', 'Arachnoiditis is a disease characterized by an acute inflammatory stage that occurs in the dura (exterior) and the arachnoid (interior), two of the three membranes that cover and protect the brain, the spinal cord and the nerve roots. The arachnoid contains the cerebrospinal fluid which circulates from the brain to the sacral area, about every two hours; it filters any invasion and usually responds first by inflammation and follows with a chronic stage life-lasting phase characterized by scarring and fibrosis. As a result, abnormal adhesion of nerve roots to the dural sac or to each other (clumping) occurs in a variety of configurations that alter significantly the function of the roots and the spinal cord. This causes a variety of neurological deficits and severe chronic neuropathic pain usually located in the area affected. In the pre-antibiotic era, severe cases of tuberculosis or syphilis invaded the spine causing arachnoiditis; currently these infections are rare, but it is important to mention that arachnoiditis will result in most patients affected by fungal meningitis from attempted epidural injections of tainted steroids.', '', NULL, NULL),
(6, 'Bloom Syndrome', 'Bloom syndrome is a rare genetic disorder characterized by short stature; increased skin sensitivity to ultraviolet rays from the sun', 'Bloom syndrome is a rare genetic disorder characterized by short stature; increased skin sensitivity to ultraviolet rays from the sun (photosensitivity); multiple small dilated blood vessels (telangiectasia) over the nose and cheeks resembling a butterfly in shape; mild immune deficiency with increased susceptibility to infections; and most importantly, a markedly increased susceptibility to many types of cancer, especially leukemia, lymphoma and gastrointestinal tract tumors. Bloom syndrome is a prototype of a group of genetic conditions known as chromosome breakage syndromes. The genetic abnormality in Bloom syndrome causes problems with DNA repair, resulting in a high number of chromosome breaks and rearrangements. The abnormal DNA repair is responsible for the increased risk for cancer.\r\n\r\n            Bloom syndrome is inherited as an autosomal recessive genetic trait. It is often included among the Jewish genetic diseases.\r\n            Infants and adults with Bloom syndrome are short and underweight and have a small head circumference, but they have normal body proportions. Affected infants and children usually present with a distinctive, narrow, small head and face. Sometimes, these signs are accompanied by a reddish facial rash that is due to the dilation of very small blood vessels (telangiectasia) of the face. The rash typically appears in a “butterfly” pattern on the cheeks and across the nose. Areas of abnormal brown or gray skin coloration (cafe-au-lait spots) may occur on other parts of the body. The skin is highly sensitive to sunlight (photosensitive) and may become very red upon exposure, especially on the face.', '', NULL, NULL),
(7, 'Enterobiasis', 'Enterobiasis or pinworm infection is a common, contagious, parasitic infestation found mainly in children.', 'Enterobiasis or pinworm infection is a common, contagious, parasitic infestation found mainly in children. The disorder is spread by swallowing or inhaling the tiny eggs of the pinworm. Enterobiasis rarely causes any serious physical problems except for the main symptom, which is severe rectal itching\r\n            The major symptom of enterobiasis is itching in the anal area. There may also be restlessness and difficulty sleeping. Secondary bacterial infections may develop in the areas that are constantly scratched and, very infrequently, the vagina may become involved in young girls. Very rarely, enterobiasis may lead to appendicitis or inflammation of the fallopian tubes in females.\r\n\r\n            Many children with enterobiasis show no symptoms (asymptomatic). In rare cases, nausea, loss of appetite, vomiting, involuntary discharge of urine at night (enuresis) or stomach pain may occur. The disorder is usually first identified when live, thin, white pinworms, (females are about 10 mm in length and males are 4-5 mm in length) are noticed in the feces.\r\n            Enterobiasis is contracted by ingesting the eggs of pinworms, which may be carried on fingernails, clothing, toys or bedding. The eggs may also be inhaled in dust. The infection may be transmitted to others by hand-to-mouth contact with contaminated food or objects.', '', NULL, NULL),
(8, 'Leukodystrophy', 'Leukodystrophies are a group of rare, progressive, metabolic, genetic diseases that affect the brain, spinal cord and often the peripheral nerves', 'Leukodystrophies are a group of rare, progressive, metabolic, genetic diseases that affect the brain, spinal cord and often the peripheral nerves. Each type of leukodystrophy is caused by a specific gene abnormality that leads to abnormal development or destruction of the white matter (myelin sheath) of the brain. The myelin sheath is the protective covering of the nerve and nerves can\'t function normally without it. Each type of leukodystrophy affects a different part of the myelin sheath, leading to a range of neurological problems.', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Andmete tõmmistamine tabelile `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_02_06_223743_create_patients_table', 1),
(2, '2019_02_07_160754_create_diseases_table', 1);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `patients`
--

CREATE TABLE `patients` (
  `patient_id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indeksid tõmmistatud tabelitele
--

--
-- Indeksid tabelile `diseases`
--
ALTER TABLE `diseases`
  ADD PRIMARY KEY (`disease_id`);

--
-- Indeksid tabelile `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeksid tabelile `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`patient_id`);

--
-- AUTO_INCREMENT tõmmistatud tabelitele
--

--
-- AUTO_INCREMENT tabelile `diseases`
--
ALTER TABLE `diseases`
  MODIFY `disease_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT tabelile `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT tabelile `patients`
--
ALTER TABLE `patients`
  MODIFY `patient_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
