<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disease extends Model
{
    //
    protected $table = 'diseases';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'c_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'disease_id',
        'name',
        'short_description',
        'full_description',
        'therapies',
        'created_at'
    ];

   
}
